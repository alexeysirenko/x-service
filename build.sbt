name := """X-Service"""
version := "1.0"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.11"

// A list of additional repositories.
// The maven central is already connected by default
resolvers ++= Seq(
  Resolver.typesafeRepo("public"),
  Resolver.sonatypeRepo("public")
)

// A list of managed library dependencies.
// Syntax: "groupId" % "artifactId" % "version" % "scope" (optional)
// Details can be found in the SBT documentation: http://www.scala-sbt.org/0.13/docs/Library-Dependencies.html
libraryDependencies ++= Seq(
  filters,
  javaWs,
  "org.apache.commons"  % "commons-lang3"         % "3.5",
  "commons-io"          % "commons-io"            % "2.5",
  "org.mockito"         % "mockito-core"          % "2.8.9"           % "test"
)

// Overrides a default configuration
javaOptions += "-Dhttp.port=9001"

