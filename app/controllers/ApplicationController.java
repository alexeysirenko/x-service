package controllers;

import actors.WordsCountActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import play.Configuration;
import play.Logger;
import play.libs.ws.WSClient;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Result;
import scala.compat.java8.FutureConverters;
import utils.Helpers;
import utils.json.SuccessResponse;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.File;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

import static actors.WordsCountProtocol.WordsCountRequest;
import static actors.WordsCountProtocol.WordsCountResponse;
import static akka.pattern.Patterns.ask;

/**
 * This controller handles requests related to the word counting.
 */
@Singleton
public class ApplicationController extends Controller {

    private final Logger.ALogger log = Logger.of(this.getClass());

    private final ActorRef wordsCountActor;

    @Inject
    public ApplicationController(ActorSystem system, WSClient wsClient, Configuration configuration) {
        this.wordsCountActor = system.actorOf(WordsCountActor.props(configuration, wsClient));
    }

    public CompletionStage<Result> upload() {
        log.info("Counting the number of words in files");
        final MultipartFormData<File> body = request().body().asMultipartFormData();
        final List<MultipartFormData.FilePart<File>> fileParts = body.getFiles();

        final List<CompletableFuture<WordsCountResponse>> responses = fileParts.stream().map(filePart -> {
            File file = filePart.getFile();
            return FutureConverters.toJava(ask(wordsCountActor, new WordsCountRequest(file), 1000 * 120))
                    .thenApply(response -> (WordsCountResponse) response)
                    .toCompletableFuture();
        }).collect(Collectors.toList());

        final CompletableFuture<Integer> maybeTotalWordsCount = Helpers.sequence(responses)
                .thenApply(wordCountsResponses ->
                    wordCountsResponses.stream()
                    .mapToInt(response -> response.wordsCount)
                    .sum());

        return maybeTotalWordsCount.thenApply(count -> ok(SuccessResponse.valueOf(count).toJson()));
    }
}
