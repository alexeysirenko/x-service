package utils.json;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.lang3.builder.ToStringBuilder;
import play.libs.Json;

import javax.annotation.Nullable;

/**
 * JSON API success document wrapper.
 */
public class SuccessResponse<T> {

    /**
     * Response data field.
     */
    public final T data;

    /**
     * Creates a new SuccessResponse instance.
     *
     * @param data response data
     */
    public static <T> SuccessResponse valueOf(@Nullable T data) {
        return new SuccessResponse<>(data);
    }

    /**
     * Returns JSON representation of the instance.
     *
     * @return JSON object
     */
    public JsonNode toJson() {
        return Json.toJson(this);
    }

    /**
     * Creates a new SuccessResponse instance.
     *
     * @param data response data
     */
    private SuccessResponse(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("data", data)
                .toString();
    }
}
