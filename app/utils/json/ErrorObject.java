package utils.json;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Represents a JSONAPI's ErrorObject.
 */
public class ErrorObject {

    @Nullable public final String id;
    @Nullable public final String status;
    @Nullable public final String code;
    @Nonnull  public final String title;
    @Nullable public final String detail;

    /**
     * Creates a new ErrorObject instance.
     *
     * @param t Throwable that caused an error
     * @return new ErrorObject
     */
    public static ErrorObject valueOf(@Nonnull Throwable t) {
        return new Builder().withTitle(t.getMessage()).build();
    }

    /**
     * Creates a new ErrorObject instance.
     *
     * @param message error message
     * @return new ErrorObject
     */
    public static ErrorObject valueOf(@Nonnull String message) {
        return new Builder().withTitle(message).build();
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("status", status)
                .append("code", code)
                .append("title", title)
                .append("detail", detail)
                .toString();
    }

    /**
     * Creates a new ErrorObject instance.
     *
     * @param builder ErrorObject Builder
     */
    private ErrorObject(Builder builder) {
        id = builder.id;
        status = builder.status;
        code = builder.code;
        title = builder.title;
        detail = builder.detail;
    }

    /**
     * {@code ErrorObject} builder static inner class.
     */
    public static final class Builder {
        private String id;
        private String status;
        private String code;
        private String title;
        private String detail;

        public Builder() {
        }

        /**
         * Sets the {@code id} and returns a reference to this Builder so that the methods can be chained together.
         *
         * @param id the {@code id} to set
         * @return a reference to this Builder
         */
        public Builder withId(String id) {
            this.id = id;
            return this;
        }

        /**
         * Sets the {@code status} and returns a reference to this Builder so that the methods can be chained together.
         *
         * @param status the {@code status} to set
         * @return a reference to this Builder
         */
        public Builder withStatus(String status) {
            this.status = status;
            return this;
        }

        /**
         * Sets the {@code code} and returns a reference to this Builder so that the methods can be chained together.
         *
         * @param code the {@code code} to set
         * @return a reference to this Builder
         */
        public Builder withCode(String code) {
            this.code = code;
            return this;
        }

        /**
         * Sets the {@code title} and returns a reference to this Builder so that the methods can be chained together.
         *
         * @param title the {@code title} to set
         * @return a reference to this Builder
         */
        public Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        /**
         * Sets the {@code detail} and returns a reference to this Builder so that the methods can be chained together.
         *
         * @param detail the {@code detail} to set
         * @return a reference to this Builder
         */
        public Builder withDetail(String detail) {
            this.detail = detail;
            return this;
        }

        /**
         * Returns a {@code ErrorObject} built from the parameters previously set.
         *
         * @return a {@code ErrorObject} built with parameters of this {@code ErrorObject.Builder}
         */
        public ErrorObject build() {
            return new ErrorObject(this);
        }
    }
}
