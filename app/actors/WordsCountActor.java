package actors;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.Scheduler;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import play.Configuration;
import play.libs.ws.WSClient;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

import java.util.concurrent.TimeUnit;

import static actors.WordsCountProtocol.*;

/**
 * Top-level supervisor actor.
 */
public class WordsCountActor extends UntypedActor {

    private final LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    private final Backoff backoff;
    private final ActorRef requestActor;

    public static Props props(Configuration configuration, WSClient wsClient) {
        return Props.create(WordsCountActor.class, configuration, wsClient);
    }

    public WordsCountActor(Configuration configuration, WSClient wsClient) {
        requestActor = getContext().actorOf(RequestActor.props(configuration, wsClient), "request-actor");
        backoff = new Backoff(
                Duration.create(1, TimeUnit.SECONDS),
                Duration.create(45, TimeUnit.SECONDS),
                0.2);
    }

    @Override
    public void preStart() {
        log.info(String.format("Starting actor %s", this));
    }

    @Override
    public void onReceive(Object message) throws Throwable {
        log.info(String.format("Received a message %s", message.toString()));

        if (message instanceof WordsCountRequest) {
            final WordsCountRequest request = (WordsCountRequest) message;
            log.info(String.format("Forwarding request %s to a child actor", message));
            final ActorRef s = sender(); // Preserve unique ask pattern's sender
            requestActor.tell(request.withSender(s), self());
        } else if (message instanceof WordsCountResponse) {
            log.info(String.format("Forwarding response %s to a sender", message));
            ((WordsCountResponse) message).request.sender.tell(message, self());
            backoff.reset();
        } else if (message instanceof ServiceErrorResponse) {
            final FiniteDuration interval = backoff.nextInterval();
            log.info(String.format("Re-sending request to a child after %d s delay", interval.toSeconds()));
            final WordsCountRequest originalRequest = ((ServiceErrorResponse) message).request;
            final Scheduler scheduler = getContext().system().scheduler();
            scheduler.scheduleOnce(interval, requestActor, originalRequest,
                    getContext().system().dispatcher(), self());
        }
    }

    @Override
    public void postStop() {
        log.info(String.format("Actor %s is stopped", this));
    }
}
