package actors;

import akka.actor.ActorRef;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.File;

/**
 * Data interaction protocol.
 */
public class WordsCountProtocol {

    public static class WordsCountRequest {
        public final File file;
        public final ActorRef sender;

        public WordsCountRequest(File file) {
            this.file = file;
            this.sender = null;
        }

        public WordsCountRequest(File file, ActorRef sender) {
            this.file = file;
            this.sender = sender;
        }

        public WordsCountRequest withSender(ActorRef sender) {
            return new WordsCountRequest(this.file, sender);
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("file", file)
                    .append("sender", sender)
                    .toString();
        }
    }

    public static class WordsCountResponse {
        public final int wordsCount;
        public final WordsCountRequest request;

        public WordsCountResponse(int wordsCount, WordsCountRequest request) {
            this.wordsCount = wordsCount;
            this.request = request;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("wordsCount", wordsCount)
                    .append("request", request)
                    .toString();
        }
    }

    public static class ServiceErrorResponse {
        public final WordsCountRequest request;
        public final Throwable t;

        public ServiceErrorResponse(WordsCountRequest request, Throwable t) {
            this.request = request;
            this.t = t;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("request", request)
                    .append("t", t)
                    .toString();
        }
    }
}
