package actors;

import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Backoff strategy planner.
 */
public class Backoff {

    public final FiniteDuration minBackoff;
    public final FiniteDuration maxBackoff;
    public final double randomFactor;

    private final AtomicLong failedAttempts = new AtomicLong(0);

    private final Random random = new Random();

    Backoff(FiniteDuration minBackoff, FiniteDuration maxBackoff, double randomFactor) {
        this.minBackoff = minBackoff;
        this.maxBackoff = maxBackoff;
        this.randomFactor = randomFactor;
    }

    public void reset() {
        failedAttempts.set(0);
    }

    public FiniteDuration nextInterval() {
        final double exp = (Math.pow(2, failedAttempts.doubleValue()) - 1) / 2;
        final long intervalInMillis = (long) (minBackoff.toMillis() + exp * 1000)
                * (100 - random.nextInt((int) (randomFactor * 100))) / 100;
        failedAttempts.incrementAndGet();

        return intervalInMillis < maxBackoff.toMillis()
                ? Duration.create(intervalInMillis, TimeUnit.MILLISECONDS)
                : maxBackoff;
    }




}
