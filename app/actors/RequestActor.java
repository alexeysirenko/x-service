package actors;

import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.io.IOUtils;
import play.Configuration;
import play.libs.Json;
import play.libs.ws.WSClient;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;

import static actors.WordsCountProtocol.*;


/**
 * Performs requests to the remote service to count the number of words in a file.
 */
public class RequestActor extends UntypedActor {

    private final LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    private final Configuration configuration;
    private final WSClient wsClient;

    public static Props props(Configuration configuration, WSClient wsClient) {
        return Props.create(RequestActor.class, configuration, wsClient);
    }

    public RequestActor(Configuration configuration, WSClient wsClient) {
        this.configuration = configuration;
        this.wsClient = wsClient;
    }

    @Override
    public void preStart() {
        log.info(String.format("Starting actor %s", this));
    }

    @Override
    public void onReceive(Object message) throws Throwable {
        log.info(String.format("Received a message %s", message));
        if (message instanceof WordsCountRequest) {
            countWords((WordsCountRequest) message).whenComplete((response, t) -> {
                if (t == null) {
                    log.info(String.format("Responding to sender with message %s", response));
                    getContext().parent().tell(response, self());
                } else {
                    log.info("Sending an error message to sender");
                    getContext().parent().tell(new ServiceErrorResponse((WordsCountRequest) message, t), self());
                }
            });
        }
    }

    @Override
    public void postStop() {
        log.info(String.format("Actor %s is stopped", this));
    }

    /**
     * Makes request to the remote service to count the number of words in a file.
     *
     * @param wordsCountRequest request
     * @return the number of words
     * @throws IOException
     */
    private CompletableFuture<WordsCountResponse> countWords(final WordsCountRequest wordsCountRequest) throws IOException {
        log.info(String.format("Counting words in file %s", wordsCountRequest.file.getAbsolutePath()));

        final String WORDS_COUNT_REQUEST_URL = configuration.getString("services.wordsCountService" );

        try(FileInputStream inputStream = new FileInputStream(wordsCountRequest.file)) {
            final String text = IOUtils.toString(inputStream, "UTF-8");
            final JsonNode payload = Json.newObject().set("text", Json.toJson(text));
            log.info(String.format("Sending file content (length: %d) to %s", text.length(), WORDS_COUNT_REQUEST_URL));

            return wsClient.url(WORDS_COUNT_REQUEST_URL)
                    .setContentType("application/json")
                    .post(payload)
                    .whenComplete((response, t) -> {
                        if (t == null) {
                            log.info(String.format("Remote server returned %s", response.asJson()));
                        } else {
                            log.info(String.format("Remote server returned an error %s", t.getMessage()));
                        }
                    })
                    .thenApply(wsResponse ->
                            new WordsCountResponse(wsResponse.asJson().get("data").asInt(), wordsCountRequest))
                    .toCompletableFuture();
        }
    }
}
