import akka.stream.javadsl.FileIO;
import akka.stream.javadsl.Source;
import akka.util.ByteString;
import com.fasterxml.jackson.databind.JsonNode;
import org.junit.Test;
import play.libs.ws.WS;
import play.libs.ws.WSClient;
import play.libs.ws.WSResponse;
import play.mvc.Http.MultipartFormData;
import play.test.TestServer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.concurrent.CompletionStage;

import static org.junit.Assert.assertEquals;
import static play.test.Helpers.*;

public class IntegrationTest {

    @Test
    public void upload() {
        final int testServerPort = play.api.test.Helpers.testServerPort();
        final TestServer server = testServer(testServerPort);
        running(server, () -> {
            try {
                try (WSClient ws = WS.newClient(testServerPort)) {
                    final File tempFile = File.createTempFile("hello", ".txt");
                    tempFile.deleteOnExit();

                    try (BufferedWriter bw = new BufferedWriter(new FileWriter(tempFile))) {
                        bw.write("Spam, egg, Spam, Spam, bacon and Spam");
                        bw.close();

                        final Source<ByteString, ?> file = FileIO.fromFile(tempFile);
                        final MultipartFormData.FilePart<Source<ByteString, ?>> fp =
                                new MultipartFormData.FilePart<>("hello", "hello.txt", "text/plain", file);
                        final MultipartFormData.DataPart dp = new MultipartFormData.DataPart("key", "value");

                        final CompletionStage<WSResponse> completionStage = ws.url("/api/upload")
                                .post(Source.from(Arrays.asList(fp, dp)));
                        final WSResponse response = completionStage.toCompletableFuture().get();
                        assertEquals(OK, response.getStatus());
                        final JsonNode data = response.asJson().get("data");
                        assertEquals("Must return a correct number of words", 7, data.asInt());
                    }
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

}
